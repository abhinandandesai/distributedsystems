package edu.rit.cs;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TCPServer {
    public static void main(String args[]) {
        try {
            int serverPort = 7896;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("TCP Server is running and accepting client connections...");
            while (true) {
                Socket clientSocket = listenSocket.accept();
                Connection c = new Connection(clientSocket);
            }
        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
    }
}

class Connection extends Thread {
    ObjectOutputStream objOut;
    ObjectInputStream objIn;

    Socket clientSocket;



    /**
     * Emit 1 for every word and store this as a <key, value> pair
     * @param allReviews
     * @return the word and it's count
     */
    public static List<KV<String, Integer>> map(List<AmazonFineFoodReview> allReviews) {
        List<KV<String, Integer>> kv_pairs = new ArrayList<KV<String, Integer>>();

        for(AmazonFineFoodReview review : allReviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                kv_pairs.add(new KV(matcher.group().toLowerCase(), 1));
        }
        return kv_pairs;
    }

    /**
     * Print the list of words and their counts
     * @param wordcount
     */
    public static StringBuilder print_word_count( Map<String, Integer> wordcount){
        StringBuilder finalList = new StringBuilder();
        for(String word : wordcount.keySet()){
            //System.out.println(word + " : " + wordcount.get(word));
            finalList.append(word).append(" : ").append(wordcount.get(word)).append("\n");

        }

        return finalList;
    }


    /**
     * count the frequency of each unique word
     * @param kv_pairs
     * @return a list of words with their count
     */
    public static Map<String, Integer> reduce(List<KV<String, Integer>> kv_pairs) {
        Map<String, Integer> results = new HashMap<>();

        for(KV<String, Integer> kv : kv_pairs) {
            if(!results.containsKey(kv.getKey())) {
                results.put(kv.getKey(), kv.getValue());
            } else{
                int init_value = results.get(kv.getKey());
                results.replace(kv.getKey(), init_value, init_value+kv.getValue());
            }
        }
        return results;
    }

    public Connection(Socket aClientSocket) {
        try {
            clientSocket = aClientSocket;
            objOut = new ObjectOutputStream(clientSocket.getOutputStream());
            objIn = new ObjectInputStream(clientSocket.getInputStream());

            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    public void run() {
        try {   // an echo server
            Object data = objIn.readObject();
            System.out.println("Received Data, Processing Word Count...");

            MyTimer myMapTimer = new MyTimer("map operation");
            myMapTimer.start_timer();
            List<KV<String, Integer>> kv_pairs = map((List<AmazonFineFoodReview>) data);
            myMapTimer.stop_timer();

            MyTimer myReduceTimer = new MyTimer("reduce operation");
            myReduceTimer.start_timer();
            Map<String, Integer> results = reduce(kv_pairs);
            myReduceTimer.stop_timer();
            myReduceTimer.print_elapsed_time();

            StringBuilder echoBackData = print_word_count(results);
            myMapTimer.print_elapsed_time();
            myReduceTimer.print_elapsed_time();

            System.out.println("Word Count completed, Echoing Back Results...");
            objOut.writeObject(echoBackData);
            System.out.println("Results Sent, Closing Connections...");
            objOut.flush();
            objIn.close();
            objOut.close();

        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                clientSocket.close();
                System.out.println("Socket Connection Closed.");
            } catch (IOException e) {/*close failed*/}
        }
    }
}

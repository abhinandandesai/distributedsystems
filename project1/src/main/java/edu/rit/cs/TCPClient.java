package edu.rit.cs;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TCPClient {

    /**
     * Read and parse all reviews
     * @param dataset_file
     * @return list of reviews
     */
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }



    public static void main(String args[]) {
        // arguments supply message and hostname of destination
        MyTimer myMapTimer = new MyTimer("Read operation");
        myMapTimer.start_timer();
        List<AmazonFineFoodReview> allReviews = read_reviews(args[0]);
        myMapTimer.stop_timer();
        myMapTimer.print_elapsed_time();
        String server_address = args[1];

        Socket s = null;
        try {
            int serverPort = 7896;
            s = new Socket(server_address, serverPort);
            System.out.println("Connecting to the Server...");
            ObjectOutputStream objOutputStream = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream objInputStream = new ObjectInputStream(s.getInputStream());
            objOutputStream.writeObject(allReviews);
            System.out.println("Reviews Read and Written in Object stream...");
            objOutputStream.flush();

            System.out.println("Review Data Sent, Awaiting Results...");
            Object results = objInputStream.readObject();
            System.out.println("Received Results...");
            System.out.println("The Word Counts are as follows:");
            System.out.println(String.valueOf(results));
            System.out.println("Closing Connections with Server...");
            objInputStream.close();
            objOutputStream.close();

        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (s != null)
                try {
                    s.close();
                    System.out.println("Socket Connection Closed.");
                } catch (IOException e) {
                    System.out.println("close:" + e.getMessage());
                }
        }
    }
}


# **CSCI.652.01 - Distributed Systems**
# **Abhinandan Desai**
# **Project 1**


# **Descriptions**
Two containers will be created in the project:
1)peer1: This is the server container. This will be executed first.
2)peer2: This is the client container. This will execute after peer1 and connect the port.

# **Working**
1) Server will start up and wait for connections.
2) Client will start and connect with the Server.
3) Client will read the file and then send it over to the Server for processing.
4) The Server will process the object and carry out the word count in the file.
5) The Server will then send the word count results to the client.
6) The Client will read the results and then print the word counts.


# **Command executions**

Compile the code
```
mvn package
```

## **TCP Socket**
Connection based method

-Xmx2048m flag is used to provide more memory when starting the container.

To start the server
```
java -Xmx2048m -cp target/project1-1.0-SNAPSHOT.jar edu.rit.cs.TCPServer
```

To run the client (first argument will be the csv file to read and the second argument will be the container name of the server)
```
java -Xmx2048m -cp target/project1-1.0-SNAPSHOT.jar edu.rit.cs.TCPClient affr.csv peer1
```

# **Docker Instructions**
To test it without rebuilding the docker image 

``` 
docker-compose up
```

To test this with rebuilding the docker image 
```
docker-compose up --build
```
